# coding=utf-8
import argparse
import json
import logging
import os

import boto3

from ib_insync import util


def load_settings(settings):
    log = logging.getLogger('configure')
    params_path = f'/trading-strategies/overnight/{settings["env"]}'

    client = boto3.client('ssm',  endpoint_url=os.environ.get('AWS_SSM_ENDPOINT'))
    params = client.get_parameters_by_path(Path=params_path)

    if params['Parameters']:
        return

    for param in settings['params']:
        param['Name'] = '/'.join((params_path, param['Name']))
        log.info(f'New SSM parameter: {param["Name"]}={param["Value"]}')
        client.put_parameter(**param)


if __name__ == '__main__':
    util.logToConsole()
    parser = argparse.ArgumentParser()
    parser.add_argument('settings_file', type=argparse.FileType('r'), help='settings template')
    args = parser.parse_args()

    load_settings(json.load(args.settings_file))