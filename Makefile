include settings.mk

build:
	docker build -t $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) .

push:
	docker tag $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) $(ECR_REPOSITORY)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	docker push $(ECR_REPOSITORY)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)

login:
	@ echo -n AWS ECR:
	@ `aws ecr get-login --profile beneficious --no-include-email`

release: build login push


test:
	docker-compose run download py.test /tests

.PHONY: \
	build \
	push \
	login \
	release \
	test

.DEFAULT_GOAL := build