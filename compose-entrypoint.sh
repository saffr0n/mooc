#!/bin/bash
set -e

python36 -u /configure.py /dev_settings.json

echo "Run: $@"
env
pwd
#py.test
exec "$@"
