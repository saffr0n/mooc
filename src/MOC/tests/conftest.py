import os
from datetime import datetime

import boto3
import pandas as pd
import pytest

from ib_insync import BarData
from MOC.calculate import (
    Calculate,
    ImproperlyConfigured
)


@pytest.fixture(scope='session')
def ssm_params():
    yield dict(bucket_name='test-bucket',
               bucket_prefix='overnight',
               companies_list_url='http://example.com')


@pytest.fixture(scope='session')
def s3_client(ssm_params):
    endpoint = os.environ.get('AWS_S3_ENDPOINT')
    assert endpoint, 'Endpoint required'

    s3 = boto3.client('s3', endpoint_url=endpoint)
    s3.create_bucket(Bucket=ssm_params['bucket_name'])

    yield s3


@pytest.fixture(scope='session')
def tws_conn():
    return "172.21.0.1", 7497


@pytest.fixture(scope='session')
def db_conn_str():
    return "dbname=postgres user=postgres host=db"


@pytest.fixture(scope='session')
def calculator(ssm_params):
    _calculator = Calculate(tws_host="22",
                            tws_port=22,
                            tws_client_id=22,
                            env='test',
                            ssm_params=ssm_params,
                            db_conn_str='dbname=postgres user=postgres host=db')
    yield _calculator


@pytest.fixture(scope='session')
def dataset():
    yield pd.DataFrame.from_records((
        dict(date='06/15/2018', open=100., high=100., low=100., close=100., volume=100., symbol='AAPL'),
    ))


@pytest.fixture(scope='session', params=[
(1, "int"),
(1., "float"),
('1.', "str"),
('1M', 'Union[ int + str]'),
('1B', 'Union[ int + str]')
])
def digits_dataset(request):
    return request.param

@pytest.fixture(scope='session')
def bars():
    yield (BarData(
        date=datetime(2017, 7, 18, 15, 30, 55),
        open=317.41,
        high=323.25,
        low=315.66,
        close=321.97,
        volume=11250,
        barCount=5895,
        average=319.677),)

@pytest.fixture(scope='session')
def config_error():
    return ImproperlyConfigured



@pytest.fixture(scope='session')
def ib():
    class test_IB:
        def __init(self):
            pass
    return test_IB
