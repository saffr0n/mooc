from MOC.calculate import Calculate
import pytest

"""нужен тест на пустые ячейки в датасете и функция заполнения их NA"""


def test_calculate_signals(calculator, dataset):
    signals = calculator.calculate_signal(dataset)
    print(signals)
    # raise

def test_get_portfolios(calculator, tws_conn):
    client_id = calculator.client_id = 1

    def commissionCallback(self, msg):
        print('-> ', msg)
        print('ID', msg.commissionReport.m_execId, 'COM', msg.commissionReport.m_commission)

    calculator.ib.setCallback('commissionReport', commissionCallback)

    calculator.ib.connect(tws_conn[0], tws_conn[1], client_id)
    # raise Exception

def test_place_orders(calculator, dataset, monkeypatch, bars):
    """"""
    calculator.get_portfolios()
    signals = calculator.calculate_signal(dataset)

    monkeypatch.setattr(calculator.ib, 'realtimeBars', lambda *args, **kwargs: bars[0])

    calculator.place_orders(signals)

def test_record_transactions_details(calculator):
    calculator.record_transactions_detail()
    raise Exception

"""and t.execution.execId not in commissions.keys()]
# or commissions.get(e.execId) == 0.0
# and e.contract.conId != 0]"""