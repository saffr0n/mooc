library(data.table)
# library(aws.s3)
library(RollingWindow)
library(TTR)

dummy_overnight <- function(n_company = 30, use_weights = TRUE, c_date = Sys.Date(), remove_ticker = NULL, ...) {
  # S3_save_bucket <- Sys.getenv('save_bucket_name')
  
  # full_data <- s3readRDS(object = "arima/real_time/data/TopPrices.RDS", bucket = S3_save_bucket)
  
  load("tmp_overnight_data.RData")
  
  # if (!is.null(remove_ticker)) {
  #   full_data <- full_data[!(ticker %in% remove_ticker),]
  #   s3saveRDS(full_data, object = "arima/real_time/data/TopPrices.RDS", bucket = S3_save_bucket)
  # }
  
  # Sharpe and RSI
  full_data <- full_data[index <= c_date,]
  
  full_data[, "Overnight.Returns" := open / shift(close, type = "lag") - 1, by = ticker]
  
  full_data <- full_data[, .SD[.N > 90], by = ticker]
  
  # Sharpe
  full_data[,
            Sharpe := RollingWindow::RollingSharpe(
              x = Overnight.Returns,
              window = 90,
              na_method = "ignore",
              scale = 252,
              Rf = rep(0, .N)
            ), by = ticker]
  
  # Overnight equity line:
  full_data[,Overnight_NA := Overnight.Returns]
  full_data[is.na(Overnight_NA), Overnight_NA := 0]
  full_data[,Overnight_equity_line :=  cumprod(Overnight_NA + 1), by = ticker]
  
  # RSI
  full_data[,
            rsi := RSI(Overnight_equity_line, n = 14),
            by = ticker]
  
  # Take only today (dummy overwrite c_date):
  c_date <- full_data[.N, index]
  latest_data <- full_data[index == c_date,]
  
  latest_data <- latest_data[, .(ticker, Sharpe, rsi)]

  signal_type <- c("Sharpe", "rsi")
  
  lapply(signal_type, function(x) {
    col_name <- paste0(x, "_rank")
    latest_data[, (col_name) := frank(-get(x), na.last = TRUE)]
  })
  
  latest_data[, total_rank := rowSums(latest_data[, paste0(signal_type, "_rank"), with = FALSE])]
  long_list <- latest_data[total_rank <= sort(total_rank, decreasing = FALSE)[n_company]]
  
  # For shorts (no negate on frank):
  lapply(signal_type, function(x) {
    col_name <- paste0(x, "_rank")
    latest_data[, (col_name) := frank(get(x), na.last = TRUE)]
  })
  
  latest_data[, total_rank := rowSums(latest_data[, paste0(signal_type, "_rank"), with = FALSE])]
  short_list <- latest_data[total_rank <= sort(total_rank, decreasing = FALSE)[n_company]]
  
  
  long_list[, side := "long"]
  short_list[, side := "short"]
  
  to_Invest <- rbind(long_list, short_list)
  
  
  if (use_weights == TRUE) {
    to_Invest[, signal_weight := 1/(total_rank) / sum(1/total_rank), by = side]
  } else {
    # equal weights
    to_Invest[, signal_weight := 1 / length(total_rank), by = side]
  }
  
  setkeyv(to_Invest, c("side", "total_rank"))
  
  # comp <- s3readRDS(object = "arima/real_time/data/top_2000_companies.RDS", bucket = S3_save_bucket)
  # setwd('/core/src/MOC')
  # file_path <- c(, )
  file_path <- paste(getwd(), "tmp_comp.RData", sep='/')
  print(file_path)    
  load("tmp_comp.RData")
  
  to_Invest[, Exchange := comp[Symbol == ticker, Exchange], by = ticker]
  to_Invest[]
}
