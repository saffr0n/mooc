import logging
import os
import datetime
import time
from tempfile import mkstemp

from rpy2.robjects import (
    pandas2ri, r
    )
import warnings
from rpy2.rinterface import RRuntimeWarning
warnings.filterwarnings("ignore", category=RRuntimeWarning)

from ib_insync import (
    wrapper, IB,
    Contract, Order,
    CommissionReport
)

import pandas as pd
import boto3
import botocore
import psycopg2

import MOC.utils
from MOC.fa_profiles import FAProfiles

class ImproperlyConfigured(Exception):
    pass


class Calculate(object):

    def __init__(self, tws_host: str, tws_port: int, tws_client_id: int,
                 env: str, ssm_params: dict, db_conn_str:str):
        self.tws_host = tws_host
        self.tws_port = tws_port
        self.client_id = tws_client_id
        self.env = env
        self.ib = IB()

        self.bucket_name = None
        self.bucket_prefix = None

        self.fa_profile_xml = 'fa.xml'
        self.db_conn_str = db_conn_str
        self._portfolios = {}
        self.transactions_details = {'contracts':[],
                                     'executions':{}}
        self.logger = logging.getLogger('MOC.calculate')
        for param in ('bucket_name', 'bucket_prefix'):
            param_value = ssm_params.get(param)
            if not param_value:
                self.logger.error(f'Cannot start download: SSM param {param} not found',
                                  extra=dict(ssm_params=ssm_params))
                raise ImproperlyConfigured(f'No SSM param: {param} in {ssm_params}')

            self.logger.info(f'{param}: {param_value}')
            setattr(self, param, param_value)

    def callback_error(self, req_id, error_code, error_str, contract):
        if error_code in (1100, 1102, 162, 165):
            # Reconnect to IB, historical data
            self.logger.info([error_code, error_str])
            return

        log = self.logger.warning if error_code in utils.IB_WARNINGS else self.logger.error
        log(f'{error_str} ({error_code})', extra=dict(error_code=error_code, req_id=req_id))

    def generate_s3_key(self) -> str:
        return '/'.join((self.bucket_prefix, self.env, datetime.today().strftime("%Y-%m-%d"),
                         'data.parquet'))

    def get_data(self) -> pd.DataFrame:
        """"""
        key_name = self.generate_s3_key()
        s3 = boto3.resource('s3', endpoint_url=os.environ.get('AWS_S3_ENDPOINT'))

        bucket = s3.Bucket(self.bucket_name)
        _, tmp_file = mkstemp()

        with open(tmp_file, 'wb') as data:
            bucket.download_fileobj(key_name, data)
            df = pd.read_csv(data)
            self.logger.info(f'Downloadet dataset: {key_name}')

        return df

    """нужен тест на пустые ячейки в датасете и функция заполнения их NA"""

    @staticmethod
    def calculate_signal(df:pd.DataFrame):
        """"""
        pandas2ri.activate()
        r_dataframe = pandas2ri.py2ri(df)
        r_script = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'sig.R')
        r.source(r_script)
        r_df = r('dummy_overnight')(len(r_dataframe)) # change it to df
        signals = pandas2ri.ri2py(r_df)
        return signals

    def check_results(self, signals):
        """"""

    def get_portfolios(self):
        """"""
        with psycopg2.connect(self.db_conn_str) as conn:
            cursor = conn.cursor()
            cursor.execute("""
            SELECT account_id, cash FROM trading.portfolios 
            WHERE strategy_id=(SELECT id FROM trading.strategies 
                               WHERE name='overnight' AND client_id=(%s))
                           """,((self.client_id),))
            accs_cash = dict(cursor.fetchall())     # {acc_id: acc_cash}
            accs_ids = tuple(accs_cash.keys())
            cursor.execute("SELECT id, account_name FROM trading.managed_accounts\
                                            WHERE id IN %s", ((accs_ids),))
            accounts = cursor.fetchall()            # {id: account_name}

        positions = self.ib.reqPositions()

        for acc in accounts:
            acc_positions = [p.position * p.avgCost for p in positions if p.account == acc[1]]

            s_nlv = sum(acc_positions) + float(accs_cash[acc[0]])

            self._portfolios[acc[1]] = {'id':acc[0],
                                        's_nlv':s_nlv,
                                        'tickers':set()}

    def place_orders(self, signals):
        """"""
        def create_contract(symbol, sec_type, exch, curr):
            contract = Contract()
            contract.symbol = symbol
            contract.secType = sec_type
            contract.exchange = exch
            contract.currency = curr
            return contract

        def create_order(order_type, quantity, action, profile):
            order = Order()
            order.orderType = order_type
            order.totalQuantity = quantity
            order.action = action
            order.faProfile = profile
            return order

        pre_orders = {}
        for ticker, row in zip(signals['ticker'], range(1, len(signals['ticker']))):
            pre_orders[ticker] = {'sgnl_wght':signals['signal_weight'][row],
                                  'exch':signals['Exchange'][row],
                                  'side':signals['side'][row]}

        for ticker, info in pre_orders.items():
            total_qtty_for_order = 0
            contract = create_contract(ticker, 'STK', info['exch'], 'USD')

            self.ib.reqRealTimeBars(contract, 5, 'TRADES', 1)
            bars = self.ib.realtimeBars()
            for p in self._portfolios.values():
                self.transactions_details[ticker] = {}
                self.transactions_details[ticker]['placed_price'] = price = bars.close
                qtty = info['sgnl_wght'] * p['s_nlv'] / price
                self.transactions_details[ticker]['qtty'] = p['qtty'] = int(qtty)

                p['tickers'].add(ticker)        # add tickers of placed orders for save it in base
                                                # (need for close orders in MOO)
                total_qtty_for_order += int(qtty)

            with open(self.fa_profile_xml, 'r') as f:
                fa_profile = FAProfiles(f.read(), self._portfolios)
                new_profile = fa_profile.generate_profile()
                self.ib.replaceFA(2, new_profile)
                profile = 'STRATEGY_OVERNIGHT_REBALANCE_PROFILE'
            f = open(self.fa_profile_xml, 'w')
            f.write(new_profile)
            f.close()

            actions = {'long': 'BUY',
                       'short': 'SELL'}
            action = actions.get(pre_orders[ticker]['side'])
            order = create_order('MKT', total_qtty_for_order, action, profile)

            self.ib.placeOrder(contract, order)

            self.transactions_details['contracts'].append(contract)
            # self.contracts.append(contract)
        self.ib.sleep(5)

    def record_transactions_detail(self):
        """"""
        non_filled = True
        while non_filled:
            non_filled = [True for t in self.ib.trades()
                          if t.orderStatus.status not in ['Filled', 'Cancelled', 'Inactive']]
            if non_filled:
                self.ib.waitOnUpdate()

        # One more check that is only self placed tickers in a set
        for acc, pfl in self._portfolios.items():
            tickers = {p.contract.symbol for p in self.ib.positions() if p.account == acc}
            pfl['tickers'] = list(pfl['tickers'] & tickers)

        # Update tickers for accounts in db
        with psycopg2.connect(self.db_conn_str) as conn:
            cursor = conn.cursor()
            for _, p in self._portfolios.items():
                cursor.execute("UPDATE trading.portfolios\
                                SET tickers=(%s) WHERE account_id=(%s)", (p['tickers'], p['id']))

            for fill in self.ib.fills():
                print()
                print('     V')
                values = (fill.contract.symbol,
                          fill.contract.secType,
                          # strike
                          fill.contract.exchange,
                          # primaryExchange,
                          fill.contract.localSymbol,
                          fill.contract.tradingClass,
                          # include_expired,
                          fill.contract.currency,
                          fill.contract.conId)

                cursor.execute("""INSERT INTO trading.contracts
                               (symbol, sec_type, exchange, local_symbol, 
                               trading_class, currency, conid)
                               VALUES (%s)""", values)

            conn.commit()

    def update_state(self):
        """"""

    def run(self):

        self.ib.setCallback('error', self.callback_error)
        self.ib.setCallback('commissionReport', self.commissionCallback)
        self.ib.connect(self.tws_host, self.tws_port, self.client_id)

        signals = self.calculate_signal(self.get_data())
        self.check_results(signals)
        self.get_portfolios()
        self.place_orders(signals)

        self.ib.disconnect()


if __name__ == '__main__':
    TWS_HOST = os.getenv("TWS_HOST")
    TWS_PORT = os.getenv("TWS_PORT")
    TWS_CLIENT_ID = os.getenv("TWS_CLIENT_ID")
    DB_CONN_STR = os.getenv("PG_CONN_STR")

    ENV = os.environ['TRADING_OVERNIGHT_ENV']

    ssm = boto3.client('ssm', endpoint_url=os.environ.get('AWS_SSM_ENDPOINT'))
    params = ssm.get_parameters_by_path(Path=f'/trading-strategies/overnight/{env}/', WithDecryption=True)
    SSM_PARAMS = dict((p['Name'].split('/')[-1], p['Value']) for p in params['Parameters'])

    app = Calculate(TWS_HOST, TWS_PORT, TWS_CLIENT_ID, ENV, SSM_PARAMS, DB_CONN_STR)
    app.run()
