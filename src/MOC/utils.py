import logging
import logging.config

import dns


def get_tws_host(params):
    service = params.get('gateway')
    if service:
        # resolver = dns.resolver.Resolver()
        answer = dns.resolver.query(f'{service}.servicediscovery.internal', 'SRV')
        rr = answer[0]
        return str(rr.target), int(rr.port)

    return 'tws', 4003


def set_logging(params):
    settings = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'formatter': 'standard',
                'level': params.get('log_level', 'ERROR'),
                'class': 'logging.StreamHandler',
            }
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'ERROR', # params.get('log_level', 'ERROR'),
                'propagate': False
            },
            'historical': {
                'handlers': ['default'],
                'level': 'DEBUG',
                'propagate': False
            },
        }
    }

    sentry_dsn = params.get('sentry_dsn')

    if sentry_dsn:
        settings['handlers']['sentry'] = {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': sentry_dsn,
        }
        settings['loggers']['historical']['handlers'] = ['default', 'sentry']

    logging.config.dictConfig(settings)


IB_WARNINGS = list(range(2100, 2109)) + [2119, 2148]

# 2119 - Market data farm is connecting
# 2148- margin compliant alert