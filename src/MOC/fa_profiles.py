from lxml import etree
from lxml.builder import E


class CannotLockProfiles(Exception):
    pass


class FAProfiles:
    def __init__(self, profiles_xml, portfolios):
        self.existing_profiles = etree.XML(profiles_xml.encode('utf-8'))
        self.portfolios = portfolios

    def generate_profile(self):
        profile = E.AllocationProfile(
            E('name', 'STRATEGY_OVERNIGHT_REBALANCE_PROFILE'),
            E('type', '2')  # Profile Method: Qtty?
        )

        accounts = E('ListOfAllocations', varName="listOfAllocations")
        # allocations = .items()
        for account, allocation in self.portfolios.items():
            accounts.append(E.Allocation(E('acct', account),
                                         E('amount', f"{allocation['qtty']:.1f}")))

        profile.append(accounts)
        return etree.tostring(
            E.ListOfAllocationProfiles(profile),
            encoding='utf-8',
            xml_declaration=True).decode().replace("\n", "")

    def get(self):
        profiles = [p for p in self.existing_profiles.findall('AllocationProfile')]
        profiles.append(self.generate_profile())

        return etree.tostring(
            E.ListOfAllocationProfiles(*profiles),
            encoding='utf-8',
            xml_declaration=True).decode().replace("\n", "")
