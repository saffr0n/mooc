FROM amazonlinux:latest
MAINTAINER Beneficious Team <tech@beneficous.com>
ARG buildreq="libcurl-devel openssl-devel gcc-c++ gcc-gfortran unzip python36-devel readline-devel pcre-devel xz-devel bzip2-devel zlib-devel libicu-devel postgresql-devel"
ARG mro_version="3.5.0"
ARG twsapi_version="973.06"
ENV LD_LIBRARY_PATH="/opt/microsoft/ropen/${mro_version}/lib64/R/lib" \
    LDFLAGS="-Wl,-rpath,/opt/microsoft/ropen/${mro_version}/lib64/R/lib" \
    R_HOME="/opt/microsoft/ropen/${mro_version}/lib64/R" \
    R_INCLUDE_DIR="/opt/microsoft/ropen/${mro_version}/lib64/R/include" \
    PYTHONPATH=/ \
    AWS_DEFAULT_REGION="us-east-1" \
    TZ="US/Eastern" \
    AWS_ACCESS_KEY_ID=none \
    AWS_SECRET_ACCESS_KEY=none \
    AWS_SSM_ENDPOINT='http://localstack:4583' \
    AWS_S3_ENDPOINT='http://localstack:4572' \
    TRADING_OVERNIGHT_ENV='dev'

COPY requirements.txt /requirements.txt
RUN yum -y install python36 util-linux libgfortran libcurl $buildreq \
    && touch /etc/redhat-release \
    && curl -o twsapi.zip https://interactivebrokers.github.io/downloads/twsapi_macunix.$twsapi_version.zip \
    && unzip twsapi.zip -d twsapi \
    && pip-3.6 install twsapi/IBJts/source/pythonclient/ \
    && rm -f twsapi.zip \
    && rm -rf twsapi \
    && curl -o microsoft-r-open.tar.gz \
        https://mran.blob.core.windows.net/install/mro/$mro_version/microsoft-r-open-$mro_version.tar.gz \
    && tar xfz microsoft-r-open.tar.gz \
    && bash microsoft-r-open/install.sh -a -u \
    && rm -rf microsoft-r-open \
    && rm -f microsoft-r-open.tar.gz \
    && rm -f /etc/redhat-release \
    && R -e 'install.packages(c("data.table", "TTR", "devtools", "curl", "httr"))' \
    && R -e 'library(devtools); install_github("andrewuhl/RollingWindow")' \
    && pip-3.6 install --no-cache -r /requirements.txt \
    && yum autoremove -y $buildreq \
    && yum clean all